clc;
clear;
%%
%translation from depth pixel (u,v,d) to a point (x,y,z)
camx = z(u) / 1000.0;
camy = (u - cx) * z / fx;
camz = (v - cy) * z / fy;

%%
x = 0.3128;         %object dimensions
y = 0.2419;
z = 0.5690;

%VGA(640x480)30fps or QVGA (320x240)60fps
px = 640/2;       %centre pixel
py = 480/2;

% http://qianyi.info/scenedata.html
f = 525; 

% We have u and v and need to find x,y,z
%we have rgbd depth

pixelu = f*x/z   %CO-ORD in image plane
pixelv = f*y/z

%%
K = [f,0,px;
     0,f,py;
     0,0,1];
X = [x;y;z;1];
P = eye(3,4); %what is P? 
% C is translation of the camera

x = K*P*X;

pixelu = x(1)/x(3)       %x/z
pixelv = x(2)/x(3)       %y/z

