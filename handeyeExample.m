function varargout = handeyeExample(varargin)
% HANDEYEEXAMPLE MATLAB code for handeyeExample.fig
%      HANDEYEEXAMPLE, by itself, creates a new HANDEYEEXAMPLE or raises the existing
%      singleton*.
%
%      H = HANDEYEEXAMPLE returns the handle to a new HANDEYEEXAMPLE or the handle to
%      the existing singleton*.
%
%      HANDEYEEXAMPLE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HANDEYEEXAMPLE.M with the given input arguments.
%
%      HANDEYEEXAMPLE('Property','Value',...) creates a new HANDEYEEXAMPLE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before handeyeExample_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to handeyeExample_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help handeyeExample

% Last Modified by GUIDE v2.5 16-Oct-2017 13:24:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @handeyeExample_OpeningFcn, ...
                   'gui_OutputFcn',  @handeyeExample_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before handeyeExample is made visible.
function handeyeExample_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to handeyeExample (see VARARGIN)

% Choose default command line output for handeyeExample
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes handeyeExample wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global pumpon 
pumpon = 0;
global dobot


% --- Outputs from this function are returned to the command line.
function varargout = handeyeExample_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pumpButton.
function pumpButton_Callback(hObject, eventdata, handles)
% hObject    handle to pumpButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global pumpon
try
    if pumpon == 0;
        pumpon = 1;
        handles.pumpText.String = 'Attempting to connect';
        msg2 = rosmessage('dobot_ros/EECtrlRequest');
        msg2.Pump = 1;
        eeSrv = rossvcclient('/dobot/ee_ctrl');
        eeSrv.call(msg2)
        pause(3);
        handles.pumpText.String = 'Pump is on';
    else
        handles.pumpText.String = '';
        pumpon = 0;
        try 
            msg2.Pump = 0;
            eeSrv.call(msg2);
        catch
            handles.pumpText.String = '';
        end  
    end
catch
    handles.pumpText.String = 'Cannot connect to pump';
end

% --- Executes on button press in pixelButton.
function pixelButton_Callback(hObject, eventdata, handles)
% hObject    handle to pixelButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Xcam
global x
global y
global z

% rgbSub = rossubscriber('/camera/rgb/image_color');
% pause(3)
% rgbimage = readImage(rgbSub.LatestMessage)
% pause(1)
rgbimage = imread('rgb.jpeg')

fig = handles.axes1
imshow(rgbimage)

handles.instructionText.String = 'Double click on location to receive pixel coord';

[v,u] = getpts(fig)
u = round(u)
v = round(v)
if length(u) > 1
    while length(u) > 1
        handles.pixelError.String = 'Too many points selected, please try again'
        [v,u] = getpts(fig)
        u = round(u)
        v = round(v)
    end
end
handles.pixelError.String = '';
handles.instructionText.String = '';
set(handles.uEdit, 'string' , num2str(u));
set(handles.vEdit, 'string' , num2str(v));
try
    camerax = x(u,v);
    cameray = y(u,v);
    cameraz = z(u,v);
    set(handles.camxEdit,'string',num2str(camerax,4));
    set(handles.camyEdit,'string',num2str(cameray,4));
    set(handles.camzEdit,'string',num2str(cameraz,4));
    Xcam = [camerax,cameray,cameraz]'
catch
    set(handles.camxEdit,'string','Error');
    set(handles.camyEdit,'string','Error');
    set(handles.camzEdit,'string','Error');
end

% --- Executes on button press in calcButton.
function calcButton_Callback(hObject, eventdata, handles)
global Xcam
% hObject    handle to calcButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

R = [0.533084567470839,0.339261728589957,-0.775062786771874;0.846048154649191,-0.218991338674430,0.486050731508841;-0.00483362587780447,-0.914846584460879,-0.403772662473746]
t = [0.758526597502126;-0.261280941854001;0.421796884198997]
try
    X = R * Xcam + t
    set(handles.robotxEdit,'string',num2str(X(1)));
    set(handles.robotyEdit,'string',num2str(X(2)));
    set(handles.robotzEdit,'string',num2str(X(3)));
catch
    set(handles.robotxEdit,'string','Error');
    set(handles.robotyEdit,'string','Error');
    set(handles.robotzEdit,'string','Error');
end




% --- Executes on button press in moveButton.
function moveButton_Callback(hObject, eventdata, handles)
% hObject    handle to moveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dobot

X = get(handles.robotxEdit,'string')
Y = get(handles.robotyEdit,'string')
Z = get(handles.robotzEdit,'string')
invalid = 0;

X = str2num(X)
Y = str2num(Y)
Z = str2num(Z)

Y = -Y % dobot model DH parameter other way round


if length(X) == 0 
    handles.moveError.String = 'Invalid entry to Robot XYZ';
    invalid = 1;
end

if invalid == 0
    dobot = Dobot
%   dobot.model.base = trotz(pi);
    handles.axes1
    qz = [0 0 0 0 0]
    dobot.model.plot3d(qz, 'workspace',dobot.workspace, 'view', 'y');
    hold on;
    target = transl(X,Y,Z) * trotx(pi)
    targetPose = dobot.model.ikcon(target)
    traj = jtraj(qz, targetPose,20)
    dobot.model.animate(traj)
end

% try
%     cartSvc = rossvcclient('dobot/cartesian');
%     cartMsg = message(cartSvc);
%     cartMsg.Pos.X = X;
%     cartMsg.Pos.Y = Y;
%     cartMsg.Pos.Z = Z;
%     cartSvc.call(cartMsg);
% catch
%     handles.moveError.String = 'Cannot connect to dobot\cartesian';
% end
   



function uEdit_Callback(hObject, eventdata, handles)
% hObject    handle to uEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of uEdit as text
%        str2double(get(hObject,'String')) returns contents of uEdit as a double


% --- Executes during object creation, after setting all properties.
function uEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function camXYZ_Callback(hObject, eventdata, handles)
% hObject    handle to camXYZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of camXYZ as text
%        str2double(get(hObject,'String')) returns contents of camXYZ as a double


% --- Executes during object creation, after setting all properties.
function camXYZ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to camXYZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function robotXYZ_Callback(hObject, eventdata, handles)
% hObject    handle to robotXYZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of robotXYZ as text
%        str2double(get(hObject,'String')) returns contents of robotXYZ as a double


% --- Executes during object creation, after setting all properties.
function robotXYZ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to robotXYZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function robotyEdit_Callback(hObject, eventdata, handles)
% hObject    handle to robotyEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of robotyEdit as text
%        str2double(get(hObject,'String')) returns contents of robotyEdit as a double


% --- Executes during object creation, after setting all properties.
function robotyEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to robotyEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function robotxEdit_Callback(hObject, eventdata, handles)
% hObject    handle to robotxEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of robotxEdit as text
%        str2double(get(hObject,'String')) returns contents of robotxEdit as a double


% --- Executes during object creation, after setting all properties.
function robotxEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to robotxEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function robotzEdit_Callback(hObject, eventdata, handles)
% hObject    handle to robotzEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of robotzEdit as text
%        str2double(get(hObject,'String')) returns contents of robotzEdit as a double


% --- Executes during object creation, after setting all properties.
function robotzEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to robotzEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function vEdit_Callback(hObject, eventdata, handles)
% hObject    handle to vEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of vEdit as text
%        str2double(get(hObject,'String')) returns contents of vEdit as a double


% --- Executes during object creation, after setting all properties.
function vEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function camyEdit_Callback(hObject, eventdata, handles)
% hObject    handle to camyEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of camyEdit as text
%        str2double(get(hObject,'String')) returns contents of camyEdit as a double


% --- Executes during object creation, after setting all properties.
function camyEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to camyEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function camxEdit_Callback(hObject, eventdata, handles)
% hObject    handle to camxEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of camxEdit as text
%        str2double(get(hObject,'String')) returns contents of camxEdit as a double


% --- Executes during object creation, after setting all properties.
function camxEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to camxEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function camzEdit_Callback(hObject, eventdata, handles)
% hObject    handle to camzEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of camzEdit as text
%        str2double(get(hObject,'String')) returns contents of camzEdit as a double


% --- Executes during object creation, after setting all properties.
function camzEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to camzEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pointcloudButton.
function pointcloudButton_Callback(hObject, eventdata, handles)
% hObject    handle to pointcloudButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global x
global y 
global z

handles.axes1
% try
%     pointsSub = rossubscriber('/camera/depth/points');
%     pause(5);
% catch
%     handles.pointcloudError.String = 'Cannot connect to /camera/depth/points';
% end
%     
% % Get the first message and plot the non coloured data
% pointMsg = pointsSub.LatestMessage
% pause(10);
% try
%     pointMsg.PreserveStructureOnRead = true;
%     pause(5)
% catch
%     handles.pointcloudError.String = 'Cannot connect to /camera/depth/points';
% end
% cloudPlot_h = scatter3(pointMsg,'Parent',gca);
% % view(40,35)
% drawnow();
% % Extract data from msg to matlab
% cloud = readXYZ(pointMsg); 
% img = readImage(rgbSub.LatestMessage);
% % Put in format to update the scatter3 plot quickly
% x = cloud(:,:,1);
% y = cloud(:,:,2);
% z = cloud(:,:,3);
% r = img(:,:,1);
% g = img(:,:,2);
% b = img(:,:,3);
% % Update the plot
% set(cloudPlot_h,'CData',[r(:),g(:),b(:)]);
% set(cloudPlot_h,'XData',x(:),'YData',y(:),'ZData',z(:));
% drawnow();

load('PointCloud.mat');
cloudPlot_h = scatter3(pointMsg,'Parent',gca);
cloud = readXYZ(pointMsg); 
x = cloud(:,:,1);
y = cloud(:,:,2);
z = cloud(:,:,3);
set(cloudPlot_h,'CData',[r(:),g(:),b(:)]);
set(cloudPlot_h,'XData',x(:),'YData',y(:),'ZData',z(:));




% --- Executes on button press in rosInit.
function rosInit_Callback(hObject, eventdata, handles)
% hObject    handle to rosInit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try 
    rosinit('138.25.49.44')
    handles.rosInit.String = 'Connected';
    
catch
    handles.rosInit.String = 'Cannot find ROS node';
end

 
