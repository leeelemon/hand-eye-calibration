 CP1 = [-0.0034,0.1490,0.7690]
 RP1 = [0.155,0.078,-0.028]
 
 CP2 = [-0.1167,0.1240,0.8270]
 RP2 = [0.12,0.0,-0.028]                            % Semi accurate
 
 CP3 = [0.0172,0.1127,0.8510]
 RP3 = [0.155,0.04,-0.028]                          % Even less accurate         
 
 A = [CP1;CP2;CP3]
 B = [RP1;RP2;RP3]                       
 
[R,t] = rigid_transform_3D(A, B)

RP1 = R*CP1' + t