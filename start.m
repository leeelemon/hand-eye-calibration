%% have to do this first
rosinit('138.25.49.44')
%% test 1 means good
find(cell2mat(strfind(rosmsg('list'),'dobot')),1)
%% list of all the topics you can subscribe to
rostopic list
%% gets colour images
rgbSub = rossubscriber('/camera/rgb/image_color');
pause(3)
%image_h = imshow(readImage(rgbSub.LatestMessage));
imwrite(readImage(rgbSub.LatestMessage), 'rbg.jpeg', 'JPEG');
%make the image larger
pause(3)
set(gcf,'units','normalized','outerposition',[0 0 1 1]);
% loop and update
tic
% while 1   
%  image_h.CData = readImage(rgbSub.LatestMessage);
%  pause(1);
%  drawnow;
%  toc;
% end
%% gets depth images
depthSub = rossubscriber('/camera/depth/image');
pause(1);
msg = depthSub.LatestMessage;
img = readImage(msg);
depthImage_h =imshow(img)
imwrite(img, 'depth.jpeg', 'JPEG');
% Make the figure large
set(gcf,'units','normalized','outerposition',[0 0 1 1])
%loop
tic
% while 1
%  depthImage_h.CData = readImage(depthSub.LatestMessage);
%  drawnow;
%  toc;
% end
%% gets current dobot state
dobotStateSub = rossubscriber('/dobot/state');
receive(dobotStateSub,2)
msg = dobotStateSub.LatestMessage;
%% To move the joints of the arm and ensure completion (remember angles are in radians)
msg = rosmessage('dobot_ros/SetPosAngRequest');
msg.RearArmAngle = 0.4;
msg.ForeArmAngle = 0.5;
msg.BaseAngle = 0;
srv = rossvcclient('/dobot/joint_angles');
srv.call(msg)
%% Note that once the above is called you could simply call the following to change the rear arm angle
msg.BaseAngle = -pi/4;
srv.call(msg)
%% To request the robot to move in Cartesian space (all in meters)
cartSvc = rossvcclient('dobot/cartesian');
cartMsg = rosmessage(cartSvc);
cartMsg.Pos.X = 0.0;
cartMsg.Pos.Y = 0.0;
cartMsg.Pos.Z = 0.02;
cartSvc.call(cartMsg);
%% Point 1
cartMsg.Pos.X = 0.6;
cartMsg.Pos.Y = 0.035;
cartMsg.Pos.Z = 0.025;
cartSvc.call(cartMsg);
%% Point 2
cartMsg.Pos.X = 0.12;
cartMsg.Pos.Y = 0.0;
cartMsg.Pos.Z = -0.025; %-0.04
cartSvc.call(cartMsg);
%% Point 3
cartMsg.Pos.X = 0.155;
cartMsg.Pos.Y = 0.04;
cartMsg.Pos.Z = -0.028;
cartSvc.call(cartMsg);
%% turn pump on
msg2 = rosmessage('dobot_ros/EECtrlRequest');
msg2.Pump = 1;
eeSrv = rossvcclient('/dobot/ee_ctrl');
eeSrv.call(msg2)
%% turn pump off
msg2.Pump = 0;
eeSrv.call(msg2);
