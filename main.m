%% have to do this first
 rosinit('138.25.49.44')
%% colour image
rgbSub = rossubscriber('/camera/rgb/image_color');
pause(3)
rgbimage = readImage(rgbSub.LatestMessage)
pause(1)
imwrite(readImage(rgbSub.LatestMessage), 'rgb.jpeg', 'JPEG');
%% depth images
% depthSub = rossubscriber('/camera/depth/image');
% pause(3);
% depthimgage = readImage(depthSub.LatestMessage);
% pause(1)
% imwrite(depthimgage, 'depth.jpeg', 'JPEG');
%% point cloud
figure(1)
pointsSub = rossubscriber('/camera/depth/points');
pause(5);
% Get the first message and plot the non coloured data
pointMsg = pointsSub.LatestMessage
pause(10);
pointMsg.PreserveStructureOnRead = true;
pause(5)
cloudPlot_h = scatter3(pointMsg,'Parent',gca);
% view(40,35)
drawnow();
% Extract data from msg to matlab
cloud = readXYZ(pointMsg); 
img = readImage(rgbSub.LatestMessage);
% Put in format to update the scatter3 plot quickly
x = cloud(:,:,1);
y = cloud(:,:,2);
z = cloud(:,:,3);
r = img(:,:,1);
g = img(:,:,2);
b = img(:,:,3);
% Update the plot
set(cloudPlot_h,'CData',[r(:),g(:),b(:)]);
set(cloudPlot_h,'XData',x(:),'YData',y(:),'ZData',z(:));
drawnow();
%% get u and v from rgbimage
fig = figure(2)
imshow(rgbimage)
[v,u] = getpts(fig)
u = round(u)
v = round(v)
%% get 3D coord from point cloud Make able to input multiple u and v's
for i=1:1:length(u)
    camerax(i) = x(u(i),v(i));
    cameray(i) = y(u(i),v(i));
    cameraz(i) = z(u(i),v(i));
    Xcam(i,:) = [camerax(i),cameray(i),cameraz(i)]
end
%% Calculate 3D coord in robotbase frame
% robot = X * Translation * Rotation
R = [0.533084567470839,0.339261728589957,-0.775062786771874;0.846048154649191,-0.218991338674430,0.486050731508841;-0.00483362587780447,-0.914846584460879,-0.403772662473746]
t = [0.758526597502126;-0.261280941854001;0.421796884198997]
X = R * Xcam + t
%% To request the robot to move in Cartesian space (all in meters)
cartSvc = rossvcclient('dobot/cartesian');
cartMsg = rosmessage(cartSvc);
cartMsg.Pos.X = X(1);
cartMsg.Pos.Y = X(2);
cartMsg.Pos.Z = X(3);
cartSvc.call(cartMsg);